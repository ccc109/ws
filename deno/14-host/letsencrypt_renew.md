

```
PS D:\ccc\ccc109\se\deno\alg\00-complexity\bigO> ssh root@misavo.com
root@misavo.com's password:
Welcome to Ubuntu 18.04.1 LTS (GNU/Linux 4.15.0-32-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue Sep 15 09:27:35 UTC 2020

  System load:  0.02               Users logged in:        0
  Usage of /:   80.5% of 24.31GB   IP address for eth0:    172.104.100.202
  Memory usage: 29%                IP address for virbr0:  192.168.122.1
  Swap usage:   41%                IP address for docker0: 172.17.0.1
  Processes:    116

 * Kubernetes 1.19 is out! Get it in one command with:

     sudo snap install microk8s --channel=1.19 --classic

   https://microk8s.io/ has docs and details.

 * Canonical Livepatch is available for installation.
   - Reduce system reboots and improve kernel security. Activate at:
     https://ubuntu.com/livepatch

367 packages can be updated.
243 updates are security updates.


*** System restart required ***
Last login: Mon Jul  6 06:08:24 2020 from 42.70.208.16
root@localhost:~# cd site
root@localhost:~/site# ls
app  backup  doc  package-lock.json  root
root@localhost:~/site# pm2 list
┌──────────┬────┬─────────┬──────┬───────┬────────┬─────────┬────────┬──────┬───────────┬──────┬──────────┐
│ App name │ id │ version │ mode │ pid   │ status │ restart │ uptime │ cpu  │ mem       │ user │ watching │    
├──────────┼────┼─────────┼──────┼───────┼────────┼─────────┼────────┼──────┼───────────┼──────┼──────────┤    
│ app      │ 6  │ 0.0.2   │ fork │ 25460 │ online │ 28      │ 2M     │ 0.1% │ 49.5 MB   │ root │ disabled │    
└──────────┴────┴─────────┴──────┴───────┴────────┴─────────┴────────┴──────┴───────────┴──────┴──────────┘    
 Use `pm2 show <id|name>` to get more details about an app
root@localhost:~/site# pm2 stop app
[PM2] Applying action stopProcessId on app [app](ids: 6)
[PM2] [app](6) ✓
┌──────────┬────┬─────────┬──────┬─────┬─────────┬─────────┬────────┬─────┬────────┬──────┬──────────┐
│ App name │ id │ version │ mode │ pid │ status  │ restart │ uptime │ cpu │ mem    │ user │ watching │
├──────────┼────┼─────────┼──────┼─────┼─────────┼─────────┼────────┼─────┼────────┼──────┼──────────┤
│ app      │ 6  │ 0.0.2   │ fork │ 0   │ stopped │ 28      │ 0      │ 0%  │ 0 B    │ root │ disabled │
└──────────┴────┴─────────┴──────┴─────┴─────────┴─────────┴────────┴─────┴────────┴──────┴──────────┘
 Use `pm2 show <id|name>` to get more details about an app
root@localhost:~/site# sudo certbot certonly --standalone
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator standalone, Installer None
Please enter in your domain name(s) (comma and/or space separated)  (Enter 'c'
to cancel): misavo.com
Cert is due for renewal, auto-renewing...
Renewing an existing certificate
Performing the following challenges:
http-01 challenge for misavo.com
Waiting for verification...
Cleaning up challenges

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/misavo.com/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/misavo.com/privkey.pem
   Your cert will expire on 2020-12-14. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le

root@localhost:~/site# pm2 start app
[PM2] Applying action restartProcessId on app [app](ids: 6)
[PM2] [app](6) ✓
[PM2] Process successfully started
┌──────────┬────┬─────────┬──────┬───────┬────────┬─────────┬────────┬─────┬──────────┬──────┬──────────┐
│ App name │ id │ version │ mode │ pid   │ status │ restart │ uptime │ cpu │ mem      │ user │ watching │      
├──────────┼────┼─────────┼──────┼───────┼────────┼─────────┼────────┼─────┼──────────┼──────┼──────────┤
│ app      │ 6  │ 0.0.2   │ fork │ 31647 │ online │ 28      │ 0s     │ 0%  │ 8.8 MB   │ root │ disabled │      
└──────────┴────┴─────────┴──────┴───────┴────────┴─────────┴────────┴─────┴──────────┴──────┴──────────┘      
 Use `pm2 show <id|name>` to get more details about an app
```

