const Shop = {}

Shop.html = `
<h2>商店首頁</h2>
<ol>
  <li><a href="#pos">新增訂單</a></li>
  <li><a href="#report">本日報表</a></li>
  <li><a href="#setting">商店設定</a></li>
</ol>
`

Shop.start = function () {
  Ui.show(Shop.html)
}
