const Pos = {
  order: {
    totalPrice: 0,
    items: [],
    submitted: false
  }
}

const Order = Pos.order

Pos.html = `
<table id="orderTable">
<thead>
  <tr>
    <td>
      <select id="product" onchange="Pos.calcPrice()"></select>
    </td>
    <td>
      <select id="addon" onchange="Pos.calcPrice()"></select>
    </td>
    <td><input id="price" type="number" value="0"></td>
    <td>
      <input id="quantity" type="number" value="1">
      <button onclick="Pos.addItem()">新增</button>
    </td>
  </tr>
  <tr><th>商品</th><th>附加</th><th>單價</th><th>數量</th></tr>
</thead>
<tbody id="orderTableBody">
  <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
</tbody>
</table>
<br/>
<div>
  <label>總價:</label>
  <input id="totalPrice" type="number" value="0">
  <button id="submit" onclick="Pos.submit()">送出訂單</button>
  <button id="abort" onclick="Pos.abort()">放棄訂單</button>
  <br/><br/>
  <button id="newOrder" onclick="Pos.start()" disabled="disabled">新增下一筆</button>
  <p><a href="#shop"> <i class="fa fa-arrow-left" aria-hidden="true"></i> 回商店</a></p>
  <br/><br/>
</div>
</div>
`

Pos.goShop = function () {
  if (!Order.submitted) {
    if (confirm('您的訂單尚未送出，請問是否要放棄該訂單？')) {
      Shop.start()
      return
    } else {
      return
    }
  }
  Shop.start()
}

Pos.abort = function () {
  if (confirm('確定要放棄本訂單？')) {
    Pos.start()
  }
}

Pos.start = function () {
  var shop = Db.load('shop')
  Ui.show(Pos.html)
  Ui.id('product').innerHTML = Pos.optionList(shop.products)
  Ui.id('addon').innerHTML = Pos.optionList(shop.addons)
  Pos.resetOrder(Order)
  Pos.calcPrice()
}

Pos.resetOrder = function (Order) {
  Order.totalPrice = 0
  Order.items = []
  Order.submitted = false
}

Pos.submit = function () {
  if (Order.items.length === 0) {
    alert('您的訂單是空的，無法送出！')
    return
  }
  Model.saveOrder(Order)
  Ui.id('submit').disabled = 'disabled'
  Ui.id('submit').innerHTML = '已送出'
  Ui.id('abort').disabled = 'disabled'
  Ui.id('newOrder').disabled = ''
}

Pos.optionList = function (list) {
  let r = []
  for (let id in list) {
    let item = list[id]
    r.push('<option value="'+id+'">'+id+'</option>')
  }
  return r.join('\n')
}

Pos.list = function () {
  let items = Order.items
  let list = []
  for (let i=0; i<items.length; i++) {
    list.push(`<tr><td>${items[i].productId}</td><td>${items[i].addonId}</td><td class="number">${items[i].price}</td><td class="number">${items[i].quantity}</td></tr>`)
  }
  return list.join('\n')
}

Pos.calcPrice = function () {
  var shop = Db.load('shop')
  let item = { product: {}, addon: {} }
  item.productId = Ui.id('product').value
  item.addonId = Ui.id('addon').value
  item.price = shop.products[item.productId].price + shop.addons[item.addonId].price
  Ui.id('price').value = item.price
  return item
}

Pos.addItem = function () {
  let item = Pos.calcPrice()
  let quantity = parseFloat(Ui.id('quantity').value)
  item.quantity = quantity
  Order.items.push(item)
  Ui.id('orderTableBody').innerHTML = Pos.list()
  Order.totalPrice += item.price * item.quantity
  Ui.id('totalPrice').value = Order.totalPrice
}
