const Tb = {}

Tb.cell2text = function (v, type) {
  if (v == null) return ''
  switch (type) {
    case 'boolean': return (v == false) ? '0' : '1'
    case 'number': return ''+v
  }
  return v
}

Tb.text2cell = function (text, type) {
  if (text.trim() === '') return undefined
  switch (type) {
    case 'boolean': return (text == true || text == "true")
    case 'number': return parseFloat(text)
  }
  return text
}

Tb.attachTo = function (selector, args) {
  document.querySelector(selector).innerHTML = Tb.table2html(args)
}

Tb.table2html = function (args) {
  var {fields, types, titles, records, styles, editable, id} = args
  var fcells = []
  for (var fi=0; fi<fields.length; fi++) {
    var title = titles[fi] || fields[fi] || ''
    fcells[fi] = `<th>${title}</th>`
  }
  var header = `<tr>${fcells.join('\n')}</tr>`
  var rows = []
  for (var record of records) {
    var cells = []
    for (var fi=0; fi<fields.length; fi++) {
      var style = (styles == null) ? '' : styles[fi]
      var cell = Tb.cell2text(record[fields[fi]], types[fi])
      cells.push(`<td contenteditable="${editable}" style="${style}">${cell}</td>`)
    }
    rows.push(`<tr>${cells.join('')}</tr>`)
  }
  var body = rows.join('\n')
  return `
<table id="${id}">
  <thead>${header}</thead>
  <tbody>${body}</tbody>
</table>`
}

Tb.updateRecords = function (args) {
  var { records, fields, types, id } = args
  var rows = document.querySelectorAll(`#${id} tr`)
  for (var ri=1; ri<rows.length; ri++) {
    var cells = rows[ri].querySelectorAll('td')
    var record = records[ri-1] || {}
    for (var ci=0; ci<cells.length; ci++) {
      var cell = cells[ci].innerText.trim()
      record[fields[ci]] = Tb.text2cell(cell, types[ci])
    }
    records[ri-1] = record
  }
  args.records = records.slice(0, rows.length-1);
  console.log(args.records)
}

Tb.addRow = function (args) {
  console.log("Tb.tableAddRow()")
  var { fields, id, editable } = args
  var tbody = document.querySelector(`#${id} tbody`)
  var row = []
  for (var fi=0; fi<fields.length; fi++) {
    row.push(`<td contenteditable="${editable}">&nbsp;</td>`)
  }
  // deleteCell = editable ? `<th><input type="button" value="X" onclick="Ui.tableDeleteRow('${args.id}', this)"></th>` : ''
  tbody.innerHTML += `<tr>${row.join('')}</tr>`
}
/*

      if (types[fi] == 'boolean') {
        cells.push(`<td style="${style}" onclick="Tb.cellClick(this, '${types[fi]}')">${cell}</td>`)
      } else {
     cells.push(`<td contenteditable="${editable}" style="${style}">${cell}</td>`)

Tb.cellClick = function (cell, type) {
  var text = cell.innerText.trim()
  // console.log('cellClick:innerText=', cell.innerText)
  switch (type) {
    case 'boolean': 
      if (text==='') cell.innerHTML = '0'
      if (text==='0') cell.innerHTML = '1'
      if (text==='1') cell.innerHTML = '&nbsp;'
      break
  }
}

Ui.tableDeleteRow = function (id, cell) {
  cell.innerText = (cell.innerText.trim() == '') ? '' : 'X'
  // var i = r.parentNode.parentNode.rowIndex
  // document.getElementById(id).deleteRow(i)
}
*/