const Cache = {
  map: new Map()
}

Cache.load = function (key) {
  var value = Cache.map.get(key)
  if (value != null) return value
}

Cache.save = function (key, value) {
  Cache.map.set(key, value)
}
