const Router = {
  list:[]
}
// ============= router ===================
Router.route = function (regexp, f) {
  Router.list.push([regexp, f])
  return this
}

Router.default = function (f) {
  Router.list.push([/.*/, f])
}

Router.onhash = async function () {
  var hash = location.hash.trim()
  console.log('onhash:hash=', hash)
  console.log('Router.list=', Router.list)
  for (let [regexp, f] of Router.list) {
    var m = hash.match(regexp)
    console.log('regexp=', regexp, 'm=', m)
    if (m) {
      f(m, hash)
      return m
    }
  }
  return null
}

Router.go = function (hash) {
  window.location.hash = hash
  return Router.onhash()
}
