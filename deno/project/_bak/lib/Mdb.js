Mdb = {}

Mdb.table = function (tbName) {
  var tb = Mdb[tbName] 
  if (tb) return tb
  Mdb[tbName] = tb = new Nedb()
  return tb
}

function promisify(func) {
  return function() {
    var args = [...arguments]
    var tbName = args.shift()
    var table = Mdb.table(tbName)
    var f = func.bind(table)
    return new Promise(function (resolve, reject) {
      // 下面這行會強制塞入 callback function, 導致 db 不傳回 cursor 而傳回 exec 解完後的值。
      args.push(function (err, result) {
        if (err) reject(err); else resolve(result);
      })
      f(...args)
    })
  }
}

Mdb.insert = promisify(Nedb.prototype.insert)
Mdb.findOne = promisify(Nedb.prototype.findOne)
Mdb.count = promisify(Nedb.prototype.count)
Mdb.update = promisify(Nedb.prototype.update)
Mdb.remove = promisify(Nedb.prototype.remove)
Mdb.ensureIndex = promisify(Nedb.prototype.ensureIndex)
Mdb.removeIndex = promisify(Nedb.prototype.removeIndex)

/*
Mdb.findOne = promisify(db.findOne.bind(db))
Mdb.count = promisify(db.count.bind(db))
Mdb.update = promisify(db.update.bind(db))
Mdb.remove = promisify(db.remove.bind(db))
Mdb.ensureIndex = promisify(db.ensureIndex.bind(db))
Mdb.removeIndex = promisify(db.removeIndex.bind(db))
*/

Mdb.find = function (tbName, q, options={}) {
  var {sort, skip, limit, projection} = options
  var table = Mdb.table(tbName)
  var cursor = table.find(q)
  if (sort) cursor = cursor.sort(sort)
  if (skip) cursor = cursor.skip(skip)
  if (limit) cursor = cursor.limit(limit)
  if (projection) cursor = cursor.projection(projection)
  return new Promise(function (resolve, reject) {
    cursor.exec(function (err, result) {
      if (err) reject(err); else resolve(result);
    })
  })
}
