
import { DB } from "https://deno.land/x/sqlite/mod.ts"

export const db = new DB("../pos.db")

db.dumpTable = function (table) {
  var r = db.query(`select * from ${table}`)
  for (const o of r) {
    console.log(o)
  }
}

db.select = function(table, fields, cond, params) {
  var rows = db.query(`SELECT ${fields.join(',')} FROM ${table} WHERE ${cond}`, params)
  var list = []
  for (const row of rows) {
    var flen = fields.length
    var o = {}
    for (let i=0; i<flen; i++) {
      o[fields[i]] = row[i]
    }
    list.push(o)
  }
  // console.log('selectAsObj:list=', list)
  return list
}
