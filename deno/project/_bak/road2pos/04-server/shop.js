import { ctxParse } from './lib.js'
import { db } from '../db.js'

const sid = 1

export const shop = {
  order: {},
}

shop.show = async function (ctx) {
  let id = ctx.params.id
  console.log('shop.show:id=', id)
  let shop = db.select('shops', ['sid', 'name', 'address', 'tel'], 'sid=?', [id])[0]
  shop.products = db.select('products', ['pid', 'name', 'price'], 'sid=?', [id])
  console.log('shop=', shop)
  ctx.response.body = shop
}
/*
shop.order.list = async function (ctx) {
  var r = db.query(`SELECT * FROM orders WHERE sid=(?)`, sid)
  var r = db.query(`SELECT oid, cid, sid, price FROM orders WHERE sid=(?)`) 
  for (const o of r) {
    console.log(o)
  }
}

shop.order.show = async function (ctx) {
  return orders[ctx.params.id]
}

shop.order.save = async function (ctx) {
  let post = ctxParse(ctx)

}
*/