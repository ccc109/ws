import { app } from '../app.js'
import { superdeno } from "https://x.nest.land/superdeno@2.2.0/mod.ts";

function server() {
  const bapp = app.handle.bind(app)
  return superdeno(bapp)
}

Deno.test("/shop/teashop", async () => {
  await server().get("/shop/teashop").expect(/茶舖子/).expect(/紅茶/)
})
