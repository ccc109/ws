const Shop = {}

Shop.html = `
<h2>商店首頁</h2>
<ol>
  <li><a href="#pos">新增訂單</a></li>
  <li><a href="#report">本日報表</a></li>
  <li><a href="#setting">商店設定</a></li>
</ol>
`

Shop.start = function () {
  Ui.show(Shop.html)
}

/*
  Model.init()
  // Ui.id('menuShopName').innerHTML = Shop.profile.name

Shop.init = function () {
  var shopJson = localStorage.getItem('Shop')
  if (shopJson) {
    Shop.profile = JSON.parse(shopJson)
  } else {
    var p = Shop.profile
    p.products = Tb.toMap(p.productList, "id")
    p.addons = Tb.toMap(p.addonList, "id")
  }
}

Shop.saveOrder = function (Order) {
  localStorage.setItem('Pos.Order.count', ++ Shop.profile.orders.count)
  Order.time = Date.now()
  Order.submitted = true
  localStorage.setItem('Pos.Order.' + Shop.profile.orders.count, JSON.stringify(Order))
}

Shop.getOrder = function (i) {
  let orderJson = localStorage.getItem('Pos.Order.'+i)
  if (orderJson == null) return null
  return JSON.parse(orderJson)
}
*/