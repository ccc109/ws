const Home = {}

Home.html = `
<h2>文學式 POS 系統</h2>

<p>這是一個《很像電子書》的系統，您可以在這裡使用下列服務：</p>

<table>
<tr><th>功能</th><th>服務</th></tr>
<tr>
  <td>開店服務</td>
  <td>
    <a href="#myshop">創建商店</a> / 
    <a href="#setting">商品設定</a> / 
    <a href="#pos">結帳POS系統</a> / 
    <a href="#report">營業報告</a>
  </td>
</tr>
<tr>
  <td>個人服務</td>
  <td>
    <a href="#market">找店</a> / 
    <a href="#signup">註冊</a> / 
    <a href="#login">登入</a> / 
    <a href="#logout">登出</a> / 
    <a href="#myprofile">我的檔案</a> / 
    <a href="#mylove">我的最愛</a>
  </td>
</tr>
</table>
`

Home.start = function () {
  Ui.show(Home.html)
}

Home.route = function () {
  var tokens = window.location.hash.split('/')
  console.log('tokens=', tokens)
  switch (tokens[0]) {
    case '#home': Home.start(); break;
    case '#market': Market.start(); break;
    case '#pos': Pos.start(); break;
    case '#report': Report.start(); break;
    case '#setting': Setting.start(); break;
    case '#shop': Shop.start(); break;
    case '#my': My.start(); break;
    case '#myorder': MyOrder.start(); break;
    case '#myprofile': MyProfile.start(); break;
    case '#myshop': MyShop.start(); break;
    case '#mylove': MyLove.start(); break;
    default: window.location.hash='#home'; break;
  }
}

window.addEventListener('load', Home.route)
window.addEventListener('hashchange', Home.route)
