const My = {}

My.html = `
<h2>用戶專區</h2>
<ol>
  <li><a href="#myorder">我的訂單</a></li>
  <li><a href="#myprofile">我的檔案</a></li>
  <li><a href="#myshop">我的商店</a></li>
  <li><a href="#mylove">我的最愛</a></li>
</ol>
`

My.start = function () {
  Ui.show(My.html)
}
