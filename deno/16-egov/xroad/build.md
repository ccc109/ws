# Build

* https://github.com/nordic-institute/X-Road/blob/develop/src/BUILD.md

## Tools

Required for building

1. OpenJDK / JDK version 8
2. Gradle
3. JRuby and rvm (ruby version manager)
4. GCC

Recommended for development environment

1. Docker (for deb/rpm packaging)
2. LXD (https://linuxcontainers.org/lxd/) -- for setting up a local X-Road instance
3. Ansible -- for automating the X-Road instance installation
