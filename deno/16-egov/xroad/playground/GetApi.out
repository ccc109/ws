HTTP/1.1 200 OK
Accept: */*
x-road-id: PLAYGROUND-0ab0806b-8835-4146-a534-786b6f70d893
x-road-client: PLAYGROUND/COM/1234567-8/TestClient
x-road-service: PLAYGROUND/GOV/8765432-1/TestService/getOpenAPI
x-road-request-id: 2c7a92f1-0a99-44d8-b127-0946d3f0ea35
x-road-request-hash: bdsitXY6xpY437NAL6tBgq64/PjVxOUKxOrbqncL5cDFp/iF/MwJgFjTokz3MvCsdkXO/ZDjbxDoKIf443w54Q==
Content-Type: application/x-yaml
Content-Length: 4272

openapi: 3.0.0
info:
  title: X-Road Simple Statistics API
  description: This API provides some basic statistics about X-Road instances - member count, Security Server count, subsystem count and list of member classes with member count per member class.
  contact:
    email: info@niis.org
  license:
    name: CC BY 4.0
    url: https://creativecommons.org/licenses/by/4.0/
  version: 1.0.0
servers:
- url: https://api.stats.x-road.global/v1
  description: X-Road Statisctics
paths:
  /instances:
    get:
      summary: get list of available instance identifiers
      description: |
        Get instance identifiers of instances which statistics are available through the API
      responses:
        200:
          description: list of instance identifiers which statistics are available
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/InstanceIdentifier'
  /instances/{instanceIdentifier}:
    get:
      summary: get current statistics
      description: |
        Get statistics related to a specific X-Road instance
      parameters:
      - name: instanceIdentifier
        in: path
        description: instance identifier of an X-Road instance
        required: true
        style: simple
        explode: false
        schema:
          type: string
      responses:
        200:
          description: statistics related to the specified X-Road instance
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/StatisticItem'
        400:
          description: bad input parameter
        404:
          description: instance not found
  /instances/{instanceIdentifier}/history:
    get:
      summary: get historical monthly statistics
      description: |
        Get historical monthly statistics related to a specific X-Road instance
      parameters:
      - name: instanceIdentifier
        in: path
        description: instance identifier of an X-Road instance
        required: true
        style: simple
        explode: false
        schema:
          type: string
      responses:
        200:
          description: historical monthly statistics related to the specified X-Road instance
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/StatisticItem'
        400:
          description: bad input parameter
        404:
          description: instance not found
components:
  schemas:
    StatisticItem:
      required:
      - date
      - instanceIdentifier
      - memberClasses
      - members
      - securityServers
      - subsystems
      type: object
      properties:
        instanceIdentifier:
          pattern: ^[\w-]{1,255}$
          type: string
          description: X-Road instance identifier
          example: FI
        subsystems:
          type: integer
          description: subsystem count
          format: int32
          example: 100
        securityServers:
          type: integer
          description: Security Server count
          format: int32
          example: 50
        members:
          type: integer
          description: member count
          format: int32
          example: 30
        date:
          pattern: ^\d{4}-\d{2}-\d{2}$
          type: string
          description: date when the data was updated
          example: 2018-08-29
        memberClasses:
          type: array
          description: list of member classes
          items:
            $ref: '#/components/schemas/MemberClass'
    MemberClass:
      required:
      - memberClass
      - memberCount
      type: object
      properties:
        memberClass:
          type: string
          description: member class name
          example: GOV
        memberCount:
          type: integer
          description: number of members in this member class
          format: int32
          example: 50
    InstanceIdentifier:
      required:
      - instanceIdentifier
      type: object
      properties:
        instanceIdentifier:
          type: string
          description: X-Road instance identifier
          example: FI
