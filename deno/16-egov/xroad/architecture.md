# X-Road Architecture

[ARC-G] X-Road Architecture
[ARC-SS] Security Server Architecture
[ARC-CP] Configuration Proxy Architecture
[ARC-CS] Central Server Architecture
[SPEC-AL] Audit Log Events
[ARC-TEC] Technologies used in X-Road
[ARC-OPMOND] Operational Monitoring Daemon Architecture
[ARC-ENVMON] Environmental Monitoring Architecture

* https://github.com/nordic-institute/X-Road/blob/develop/doc/Architecture/arc-g_x-road_arhitecture.md

* [Figure 1. Logical structure of X-Road](./img/arc-g_logical_structure_of_x_road.png)

* [Figure 2. Deployment view of X-Road](./img/arc-g_deployment_view_of_x_road.png)

## X-Road: Security Server Architecture

* https://github.com/nordic-institute/X-Road/blob/develop/doc/Architecture/arc-ss_x-road_security_server_architecture.md

* [Figure 1. Security server component diagram](./img/arc-ss_security_server_component_diagram.svg)

* [Figure 2. Security server process diagram](./img/arc-ss_security_server_process_diagram.svg)

* [Figure 3. Simple security server deployment](./img/arc-ss_simple_security_server_deployment.svg)

## X-Road: Configuration Proxy Architecture

* https://github.com/nordic-institute/X-Road/blob/develop/doc/Architecture/arc-cp_x-road_configuration_proxy_architecture.md

* [Figure 1. Configuration proxy component diagram](../img/arc-cp_configuration_proxy_component_diagram.png)
* [Figure 2. Configuration creation sequence diagram](../img/arc-cp_configuration_creation_sequence_diagram.png)
* [Figure 3. Configuration proxy deployment example](../img/arc-cp_configuration_proxy_deployment_example.png)


## X-Road: Central Server Architecture

* https://github.com/nordic-institute/X-Road/blob/develop/doc/Architecture/arc-cs_x-road_central_server_architecture.md

* [Figure 1. Components and interfaces of the X-Road central server](../img/arc-cs_components_and_interfaces_of_the_x_road_central_server.png)
* [Figure 2. The process of global configuration generation and distribution](../img/arc-cs_the_process_of_global_configuration_and_distribution.png)
* [Figure 3. Simple deployment of the central server](../img/arc-cs_simple_deployment_of_the_central_server.png)

## X-Road technologies

[X-Road 使用的相關技術圖](https://nordic-institute.github.io/X-Road-tech-radar/) (重要！)

* Java 8
* Logback -- Logging Framework
* Akka 2 -- Akka is a toolkit for building highly concurrent, distributed, and resilient message-driven applications for Java and Scala
* Jetty 9 -- Java Web Servlet Framework
* Ubuntu 18.04 
* PostgreSQL 10
* nginx
* PAM -- Pluggable Authentication Modules (Linux)
* Liquibase 3 -- Open Source Version Control for Your Database
* systemd -- systemd is a system and service manager for Linux operating systems. systemctl is a command to introspect and control the state of the systemd system and service manager.
* PKCS #11 -- Public Key Cryptography Standards". These are a group of public-key cryptography standards devised and published by RSA Security LLC, starting in the early 1990s.
* Dropwizard Metrics 4 -- Metrics provides a powerful toolkit of ways to measure the behavior of critical components in your production environment.
* Spring Boot 2 -- Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".
* Vue.js 2
* Npm 6
* Node 12
* Typescript
* OpenAPI 3 -- a broadly adopted industry standard for describing modern APIs.
* Embedded Tomcat 9 --  embedding Tomcat server into Java applications

HOLD:

* Red Hat Enterprise Linux 7 (RHEL7)
* C
* JRuby 9 -- https://www.jruby.org/
* PostgreSQL 9.4

2 Overview matrix of the X-Road technology

[Table 1](#Ref_Technology_matrix_of_the_X_Road) presents the list of technologies used in the X-Road and mapping between the technologies and X-Road components.

<a id="Ref_Technology_matrix_of_the_X_Road" class="anchor"></a>
Table 1. Technology matrix of the X-Road

 **Technology**                     | **Security server** | **Central server** | **Configuration proxy** | **Operational Monitoring Daemon**
----------------------------------- | ------------------- | ------------------ | ----------------------- | -------------------
 Java 8                             | X                   | X                  | X                       | X
 C                                  | X                   | X                  |                         |
 Logback                            | X                   | X                  | X                       | X
 Akka 2                             | X                   | X                  | X                       | X
 Jetty 9                            | X    | X                  |                         |
 JRuby 9                            |                     | X                  |                         |
 Ubuntu 18.04                       | X                   | X                  | X                       | X
 Red Hat Enterprise Linux 7 (RHEL7) | X                   |                    |                         | X
 PostgreSQL 9.4                     |                     | X   |                         |
 PostgreSQL 10                      | X                   | X                  |                         | X
 nginx                              |                     | X                  | X                       |
 PAM                                | X                   | X                  |                         |
 Liquibase 3                        | X                   | X                  |                         | X
 systemd                            | X                   | X                  | X                       | X
 PKCS            | X                   | X                  | X                       |
 Dropwizard Metrics 4               | X                   |                    |                         | X
 Spring Boot 2                      | X                   |                    |                         |  
 Vue.js 2                           | X                   |                    |                         |  
 Npm 6                              | X                   |                    |                         |  
 Node 12                            | X                   |                    |                         |  
 Typescript                         | X                   |                    |                         |  
 OpenAPI 3                          | X                   |                    |                         |  
 Embedded Tomcat 9                  | X                   |                    |                         |  

## X-Road: Operational Monitoring Daemon Architecture

* https://github.com/nordic-institute/X-Road/blob/develop/doc/OperationalMonitoring/Architecture/arc-opmond_x-road_operational_monitoring_daemon_architecture_Y-1096-1.md

* [Figure 1. Operational monitoring daemon component diagram](../img/x-road_operational_monitoring_daemon_components.png)
* [Figure 2. Operational monitoring daemon deployment](../img/x-road_operational_monitoring_daemon_deployment.png)

A.2 Example Store Operational Monitoring Data Request

The first record of the store request reflects successfully mediated request, the second one unsuccessfully mediated request.

```json
{
  "records": [
    {
      "monitoringDataTs": 1576133363,
      "securityServerInternalIp": "fd42:2642:2cb3:31ac:216:3eff:fedf:85c%eth0",
      "securityServerType": "Client",
      "requestInTs": 1576133360081,
      "requestOutTs": 1576133361160,
      "responseInTs": 1576133361818,
      "responseOutTs": 1576133361876,
      "clientXRoadInstance": "FI",
      "clientMemberClass": "COM",
      "clientMemberCode": "111",
      "clientSubsystemCode": "CLIENT",
      "serviceXRoadInstance": "FI",
      "serviceMemberClass": "COM",
      "serviceMemberCode": "111",
      "serviceSubsystemCode": "SERVICE",
      "serviceCode": "getRandom",
      "serviceVersion": "v1",
      "messageId": "1234",
      "messageUserId": "1234",
      "messageIssue": "1234",
      "messageProtocolVersion": "4.x",
      "clientSecurityServerAddress": "ss1",
      "serviceSecurityServerAddress": "ss1",
      "requestSize": 1226,
      "responseSize": 1539,
      "requestAttachmentCount": 0,
      "responseAttachmentCount": 0,
      "succeeded": true,
      "xRequestId": "d4490e7f-305e-44c3-b869-beaaeda694e7",
      "serviceType": "WSDL"
    },
    {
      "monitoringDataTs": 1576134508,
      "securityServerInternalIp": "fd42:2642:2cb3:31ac:216:3eff:fedf:85c%eth0",
      "securityServerType": "Client",
      "requestInTs": 1576134507705,
      "requestOutTs": 1576134507840,
      "responseInTs": 1576134508040,
      "responseOutTs": 1576134508045,
      "clientXRoadInstance": "FI",
      "clientMemberClass": "COM",
      "clientMemberCode": "111",
      "serviceXRoadInstance": "FI",
      "serviceMemberClass": "COM",
      "serviceMemberCode": "111",
      "serviceCode": "getSecurityServerHealthData",
      "serviceVersion": "v1",
      "messageId": "1234",
      "messageProtocolVersion": "4.x",
      "clientSecurityServerAddress": "ss1",
      "serviceSecurityServerAddress": "ss1",
      "requestSize": 1767,
      "requestAttachmentCount": 0,
      "succeeded": false,
      "faultCode": "Server.ServerProxy.OpMonitor.InvalidClientIdentifier",
      "faultString": "Missing required subsystem code",
      "xRequestId": "2c51b181-47cd-4ff2-b5df-6463f968fd0c",
      "serviceType": "WSDL"
    }   
  ]
}
```

* Example of response indicating success.

```json
{
  "status": "OK"
}
```

* Example of response indicating failure.

```json
{
  "status": "Error",
  "errorMessage": "Internal error"
}
```

## X-Road: Environmental Monitoring Architecture

* https://github.com/nordic-institute/X-Road/blob/develop/doc/EnvironmentalMonitoring/Monitoring-architecture.md

* [Components](../img/monitoring.png)
* [JMX interface](../img/monitoring-jmx.png)

## 7 Technical terms

* https://github.com/nordic-institute/X-Road/blob/develop/doc/Architecture/arc-g_x-road_arhitecture.md

### 7.1 Trust and security terminology

**CA** - Certification Authority    

**HSM** – Hardware security module

**OCSP** – Online Certificate Status Protocol 

**SSH** - Secure Shell

**TLS** - Transport Layer Security

**TSA** - Timestamping Authority 

**TSP** - Time Stamp Provider

### 7.2 General software terminology

**API** Application Programming Interface

**CI** - Continuous Integration

**DSL** - Domain Specific Language

**HTTP** - Hypertext Transfer Protocol  

**HTTPS** - Hypertext Transfer Protocol Secure

**JMX** - The Java Management Extensions  
  
**JMXMP** - Java Management Extensions Messaging Protocol
  
**JSON** - JavaScript Object Notation  

**MBean** - Java Managed Bean  

**MIME** - Multipurpose Internet Mail Extensions

**RPC** – Remote Procedure Call

**REST** - Representational State Transfer

**SDK** - Software Development Kit

**SOAP** - Simple Object Access Protocol  
