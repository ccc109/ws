# X-Road: Protocol

[PR-REST] Message Protocol for REST
[PR-MESS] Message Protocol
[PR-MESSTRANSP] Message Transport Protocol
[PR-MSERV] Protocol for Management Services
[PR-META] Service Metadata Protocol
[PR-GCONF] Protocol for Downloading Configuration
[PR-TARGETSS] Security Server Targeting Extension for the X-Road Message Protocol
[PR-SECTOKEN] Security Token Extension for the X-Road Message Protocol
[PR-OPMON] Operational Monitoring Protocol
[PR-OPMONJMX] Operational Monitoring JMX Protocol
[PR-ENVMONMES] Environmental Monitoring Messages

## X-Road: Message Protocol for REST

* https://github.com/nordic-institute/X-Road/blob/develop/doc/Protocols/pr-rest_x-road_message_protocol_for_rest.md

This document describes the X-Road Message Protocol for [REST]. The protocol is used in X-Road infrastructure between information systems and X-Road Security Servers to consume and produce REST services. Between the Security Servers there is another protocol called X-Road Message Transport Protocol which is described in its own document [PR-MESSTRANSP].

* [](../img/message-protocol-for-rest.png)

* [](../img/)
* [](../img/)
* [](../img/)
* [](../img/)
* [](../img/)
* [](../img/)
* [](../img/)
* [](../img/)
* [](../img/)
* [](../img/)
* [](../img/)
* [](../img/)
