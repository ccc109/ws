# classpath

* https://stackoverflow.com/questions/219585/including-all-the-jars-in-a-directory-within-the-java-classpath

```
Windows:

 java -cp file.jar;dir/* my.app.ClassName
Linux:

 java -cp file.jar:dir/* my.app.ClassName
Remind:
- Windows path separator is ;
- Linux path separator is :
- In Windows if cp argument does not contains white space, the "quotes" is optional
```